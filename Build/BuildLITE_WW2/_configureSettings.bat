REM Time range
set SH_Date=20171027


REM Path
set SH_TempFolder=t:\Temp_BuildLITE

set _TARGETPATH_BASE_Assets_LQ=P:\WW2\Assets_t
set _SOURCEPATH_BASE_Assets_IF=P:\WW2\Assets_t_SourceIF
set _SOURCEPATH_BASE_Assets_WW2=P:\WW2\Assets_t_SourceWW2

set _TARGETPATH_BASE_Core_LQ=P:\WW2\Core_t
set _SOURCEPATH_BASE_Core_IF=P:\WW2\Core_t_SourceIF
set _SOURCEPATH_BASE_Core_WW2=P:\WW2\Core_t_SourceWW2

set _TARGETPATH_OBJECTS_LQ=P:\WW2\Objects_t
set _SOURCEPATH_OBJECTS_IF=P:\WW2\Objects_t_SourceIF
set _SOURCEPATH_OBJECTS_WW2=P:\WW2\Objects_t_SourceWW2

set _TARGETPATH_TERRAINSIF_LQ=P:\WW2\TerrainsIF_t
set _SOURCEPATH_TERRAINSIF=P:\WW2\TerrainsIF_t_Source

set _SOURCEPATH_BASE=D:\kju\release\B\@WW2_FULL



REM Tools
set PAL2PACE_EXE=D:\kju\Programs\A3T\ImageToPAA\ImageToPAA.exe


REM Date
set MYTAG_YEAR=%date:~-4%
REM set MYTAG_MONTH=%date:~-7,2%
REM set MYTAG_DAY=%date:~-10,2%

set MYTAG_MONTH=%date:~-10,2%
set MYTAG_DAY=%date:~-7,2%